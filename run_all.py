# encoding=utf-8
"""
"""
import utilities
import datetime
import luigi
import service_tables

pg_server = '173'

ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


@luigi.Task.event_handler(luigi.Event.START)
def task_start(self):
    utilities.luigi_log_start(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)


@luigi.Task.event_handler(luigi.Event.SUCCESS)
def task_success(self):
    utilities.luigi_log_pass(pg_server, self.task_id + '__' + ts_no_spaces, self.pipeline, self.__class__.__name__)
    try:  # WrapperTest throws an AttributeError, see /Notes/issues
        with self.output().open("w") as out_file:
            # write to the output file
            out_file.write('pass')
    except AttributeError:
        pass


class RunAll(luigi.WrapperTask):
    """

    """
    pipeline = 'run_all'

    def requires(self):
        yield service_tables.DimTech()


if __name__ == '__main__':
    luigi.run(["--workers=6"], main_task_cls=RunAll)
