# encoding=utf-8
import psycopg2
import pyodbc
import pypyodbc

# connections ####################################################


def pg(server):
    # if server == '173':  # !!!!!!!!!!!!1 THIS IS FOR TESTING ON LOCAL ONLY, POINTS 173 TO LOCALHOST!!!!!!!!!!!!!!!!!1
    #     # return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")
    #     # 173 is now pointing to pg_standby with a current restored backup from 173
    #     # for testing the deployment of the new python3 luigi
    #     return psycopg2.connect("host='10.130.196.209' dbname='cartiva' user='postgres' password='cartiva'")
    if server == 'local':
        return psycopg2.connect("host='localhost' dbname='cartiva' user='postgres' password='cartiva'")
    elif server == '173':
        return psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
    elif server == '174':
        return psycopg2.connect("host='10.130.196.174' dbname='cartiva' user='postgres' password='cartiva'")
    elif server == '88':
        return psycopg2.connect("host='10.130.196.88' dbname='cartiva' user='rydell' password='cartiva'")
    elif server == '73':
        return psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
    elif server == '139':  # jon's local vm for luigi dev
        return psycopg2.connect("host='192.168.43.139' dbname='Cartiva' user='postgres' password='cartiva'")


def mysql_shoretel_config():
    return pyodbc.connect("Provider=MSDASQL; DRIVER={MySQL ODBC 5.2a Driver};SERVER=192.168.100.10; "
                          "Port=4308;DATABASE=shoreware;USER=st_configread;PASSWORD=passwordconfigread;OPTION=3;")


def drive_centric():
    return pyodbc.connect('Driver={SQL Server}; Server=52.22.117.38;Database=leadcrumb_daily_copy;'
                          'uid=store-access;pwd=h%Mf4Ze5L#yQ9n*')


def arkona(server):
    if server == 'report':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    elif server == 'production':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver};system=RYDELL.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


def arkona_luigi_27(server):
    if server == 'report':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver 64-bit};system=REPORT1.ARKONA.COM;uid=rydejon;pwd=fuckyou5')
    elif server == 'production':
        return pypyodbc.connect(
            'DRIVER={iSeries Access ODBC Driver 64-bit};system=RYDELL.ARKONA.COM;uid=rydejon;pwd=fuckyou5')


def ads_sco():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\scotest\\sco.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def ads_dds():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\dds\\dds.add;UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def ads_dps():
    return pyodbc.connect('DRIVER={Advantage StreamlineSQL ODBC};'
                          'DataDirectory=\\\\67.135.158.12:6363\\advantage\\dpsVSeries\\dpsVSeries.add;'
                          'UID=adssys;PWD=cartiva;'
                          'TrimTrailingSpaces=True')


def luigi_log_start(server, log_id, pipeline, task):
    """
        in luigi, don't need to return the run_id, just log the task as having started
    """
    with pg(server) as con:
        with con.cursor() as cur:
            sql = """
              insert into luigi.luigi_log (log_id, pipeline, task, status)
              values('%s','%s','%s','started')
            """ % (log_id, pipeline, task)
            cur.execute(sql)


def luigi_log_pass(server, log_id, pipeline, task):
    with pg(server) as con:
        with con.cursor() as cur:
            sql = """
                update luigi.luigi_log
                set thru_ts = (select current_timestamp),
                    status = 'pass'
                where pipeline = '%s'
                  and log_id = '%s'
                  and task = '%s'
            """ % (pipeline, log_id, task)
            cur.execute(sql)


def luigi_log_error(server, _run_id, _message):
    with pg(server) as con:
        with con.cursor() as cur:
            sql = """
                update ops.luigi_task_log
                set run_status = 'Error',
                    message = '%s'
                where run_id = '%s'
            """ % (_message, _run_id)
            cur.execute(sql)
