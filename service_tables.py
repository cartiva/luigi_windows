# coding=utf-8
"""

"""

import luigi
import utilities
import csv
import datetime

pipeline = 'service_tables'
pg_server = '173'
ts_no_spaces = str(datetime.datetime.now()).replace(" ", "")


class DimTech(luigi.Task):
    """

    """
    extract_file = 'extract_files/dim_tech.csv'
    pipeline = pipeline

    def local_target(self):
        return ('output_files/' + '{:%Y-%m-%d}'.format(datetime.datetime.now()) + '_' +
                self.__class__.__name__ + ".txt")

    def requires(self):
        return None

    def output(self):
        # define output filename and path
        return luigi.LocalTarget(self.local_target())

    def run(self):
        with utilities.ads_dds() as ads_con:
            with ads_con.cursor() as ads_cur:
                sql = """select * from dimTech;"""
                ads_cur.execute(sql)
                with open(self.extract_file, 'w') as f:
                    csv.writer(f).writerows(ads_cur.fetchall())
